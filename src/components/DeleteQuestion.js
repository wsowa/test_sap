import React from "react";
import axios from "axios";
import { Link } from "react-router-dom";

class DeleteQuestion extends React.Component {
  constructor(props) {
    super(props);
    this.deleteHandler = this.deleteHandler.bind(this);
    this.state = {
      idd: props.id
    };
  }
  //   console.log("to: " + props.id);
  //   axios
  //     .delete("http://www.testapi.pl/api/questions/" + props.id)
  //     .then(response => {
  //       console.log(response);

  //     });

  deleteHandler() {
    console.log("to: " + this.state.idd);
    axios
      .delete("http://www.testapi.pl/api/questions/" + this.state.idd)
      .then(response => {
        console.log(response);
      });
  }
  render() {
    return (
      <>
        {console.log("toxx: " + this.state.idd)}
        <button onClick={this.deleteHandler}>
          <Link className="pytanie" to="/questionspanel/wyswietl">
            tak
          </Link>
        </button>
        <button>
          <Link className="pytanie" to="/questionspanel/wyswietl">
            nie
          </Link>
        </button>
      </>
    );
  }
}
export default DeleteQuestion;
