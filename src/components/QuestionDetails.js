import React from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import "../styles/questiondetails.css";

class QuestionDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      Questions: {
        id: "",
        questionCont: "",
        isOpen: "",
        answers: [
          {
            id: "",
            answerCont: "",
            isCorrect: ""
          }
        ]
      },
      idd: props.id
    };
  }

  componentDidMount() {
    axios
      .get("http://www.testapi.pl/api/questions/" + this.state.idd)
      .then(res => {
        const Questions = res.data;
        this.setState({ Questions });
      });
  }

  render() {
    var counter = 1;
    const list = this.state.Questions.answers.map(answer => (
      <tr>
        <td>{counter++}</td>
        <td>{answer.answerCont}</td>
        <td>{answer.isCorrect === 1 ? "tak" : "nie"}</td>
      </tr>
    ));
    return (
      <div>
        <ul className="pytanie">
          <li>
            {this.state.Questions.isOpen === 1
              ? "Pytanie otwarte"
              : "Pytanie zamknięte"}
          </li>
          <br />
          <li>{"Treść: " + this.state.Questions.questionCont}</li>
        </ul>
        <table id="questions">
          <thead>
            <tr>
              <th>#</th>
              <th>Ddpowiedź</th>
              <th>Poprawna</th>
            </tr>
          </thead>
          <tbody>{list}</tbody>
        </table>
        <Link className="pytanie" to="/questionspanel/wyswietl">
          <button className="pytanie">Wróć do listy</button>
        </Link>
      </div>
    );
  }
}
export default QuestionDetails;
