import React from "react";
import img from "../images/zf.PNG";
import "../styles/header.css";

const Header = () => {
  return (
    <div>
      <img src={img} alt="zf" />
    </div>
  );
};

export default Header;
