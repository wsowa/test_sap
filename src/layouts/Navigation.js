import React from "react";
import "../styles/navigation.css";
import { NavLink } from "react-router-dom";

const list = [
  { name: "Home", path: "/", exact: true },
  { name: "Test", path: "/test" },
  { name: "Okno live", path: "/livewindow" },
  { name: "Panel", path: "/admin" }
];
const Navigation = () => {
  const menu = list.map(item => (
    <li key={item.name}>
      <NavLink to={item.path} exact={item.exact ? item.exact : false}>
        {item.name}
      </NavLink>
    </li>
  ));

  return (
    <nav className="main">
      <ul>{menu}</ul>
    </nav>
  );
};

export default Navigation;
