import React from "react";
import { Route, Switch } from "react-router-dom";
import HomePage from "../pages/HomePage";
import TestPage from "../pages/TestPage";
import LiveWindowPage from "../pages/LiveWindowPage";
import LoginPage from "../pages/LoginPage";
import AdminPage from "../pages/AdminPage";

import QuestionsPanelListPage from "../pages/QuestionsPanelListPage";

import ErrorPage from "../pages/ErrorPage";

const Page = () => {
  return (
    <>
      <Switch>
        <Route path="/" exact component={HomePage} />
        <Route path="/test" component={TestPage} />
        <Route path="/livewindow" component={LiveWindowPage} />
        <Route path="/questionspanel" component={QuestionsPanelListPage} />
        <Route path="/login" component={LoginPage} />
        <Route path="/admin" component={AdminPage} />
        <Route component={ErrorPage} />
      </Switch>
    </>
  );
};

export default Page;
