import React from "react";
import { NavLink } from "react-router-dom";
import "../styles/questionnavigation.css";

const panel = ["dodaj", "wyswietl"];
const QuestionNavigation = () => {
  const list = panel.map(question => (
    <li key={question}>
      <NavLink
        to={`/questionspanel/${question}`}
        exact={question.exact ? question.exact : false}
      >
        {question}
      </NavLink>
    </li>
  ));
  return (
    <nav className="inside">
      <ul>{list}</ul>
    </nav>
  );
};

export default QuestionNavigation;
