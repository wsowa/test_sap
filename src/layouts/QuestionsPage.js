import React from "react";
import { Route, Switch } from "react-router-dom";
import AddQuestionPage from "../pages/AddQuestionPage";
import ShowQuestionsPage from "../pages/ShowQuestionsPage";
import QuestionDetailsPage from "../pages/QuestionDetailsPage";
import EditQuestionsPage from "../pages/EditQuestionsPage";
import DeleteQuestionsPage from "../pages/DeleteQuestionsPage";

const QuestionsPage = () => {
  return (
    <Switch>
      <Route path="/questionspanel/dodaj" exact component={AddQuestionPage} />
      <Route
        path="/questionspanel/wyswietl"
        exact
        component={ShowQuestionsPage}
      />
      <Route
        path="/questionspanel/wyswietl/:id"
        exact
        component={QuestionDetailsPage}
      />
      <Route
        path="/questionspanel/edycja/:id"
        exact
        component={EditQuestionsPage}
      />
      <Route
        path="/questionspanel/usun/:id"
        exact
        component={DeleteQuestionsPage}
      />
    </Switch>
  );
};

export default QuestionsPage;
