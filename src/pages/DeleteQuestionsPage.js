import React from "react";
import axios from "axios";

import DeleteQuestion from "../components/DeleteQuestion";

const DeleteQuestionsPage = ({ match }) => {
  return (
    <>
      <DeleteQuestion id={match.params.id} />
    </>
  );
};

export default DeleteQuestionsPage;
