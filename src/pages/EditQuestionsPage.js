import React from "react";

import EditQuestion from "../components/EditQuestion";

const EditQuestionsPage = ({ match }) => {
  return (
    <>
      <EditQuestion id={match.params.id} />
    </>
  );
};

export default EditQuestionsPage;
