import React from "react";
import { Route, Redirect } from "react-router-dom";
import "../styles/loginpage.css";

class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.onChangeLogin = this.onChangeLogin.bind(this);
    this.onChangePassword = this.onChangePassword.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.state = {
      login: "",
      password: "",
      permission: false,
      badpassword: false
    };
  }
  onChangeLogin(e) {
    this.setState({
      login: e.target.value
    });
  }
  onChangePassword(e) {
    this.setState({
      password: e.target.value
    });
  }
  onSubmit(e) {
    e.preventDefault();
    const login = this.state.login;
    const password = this.state.password;
    login === "zfadmin" && password === "zfadmin"
      ? this.setState({ permission: true })
      : this.setState({
          permission: false,
          badpassword: true,
          login: "",
          password: ""
        });
  }

  render() {
    return (
      <>
        <form onSubmit={this.onSubmit}>
          <div className="logowanie">
            <label htmlFor="">Login:</label>
            <input
              value={this.state.login}
              onChange={this.onChangeLogin}
              type="text"
              placeholder="Wpisz login..."
            />
            <label htmlFor="">Hasło:</label>
            <input
              value={this.state.password}
              onChange={this.onChangePassword}
              type="password"
              placeholder="Wpisz hasło..."
            />
            <button className="log">Zaloguj</button>
          </div>
        </form>
        <Route
          render={() =>
            this.state.permission ? (
              <Redirect to="/questionspanel" />
            ) : (
              <div className="bladlogowania">
                {this.state.badpassword ? (
                  <div>
                    Brak dostępu <br />
                    <br />
                    Błędne hasło }
                  </div>
                ) : (
                  <div />
                )}
              </div>
            )
          }
        />
      </>
    );
  }
}

export default LoginPage;
