import React from "react";

import QuestionDetails from "../components/QuestionDetails";

const QuestionDetailsPage = ({ match }) => {
  return (
    <>
      <QuestionDetails id={match.params.id} />
    </>
  );
};

export default QuestionDetailsPage;
