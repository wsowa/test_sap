import React from "react";

import "../styles/questionpanellist.css";
import QuestionsPage from "../layouts/QuestionsPage";
import QuestionNavigation from "../layouts/QuestionNavigation";

const QuestionsPanelListPage = () => {
  return (
    <div className="mainInside">
      <section className="naviInside">{<QuestionNavigation />}</section>
      <section className="pageQuestions">{<QuestionsPage />}</section>
    </div>
  );
};

export default QuestionsPanelListPage;
