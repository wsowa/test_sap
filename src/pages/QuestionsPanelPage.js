import React from "react";
import QuestionsNavigation from "../layouts/QuestionNavigation";
import QuestionsPage from "../layouts/QuestionsPage";
const QuestionsPanelPage = () => {
  return (
    <>
      <div className="navi">{<QuestionsNavigation />}</div>
      <div className="qPage">{<QuestionsPage />}</div>
    </>
  );
};

export default QuestionsPanelPage;
