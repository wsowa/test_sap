import React from "react";
import "../styles/showquestionspage.css";
import axios from "axios";
import { Link } from "react-router-dom";
import info_img from "../images/info.png";
import edit_img from "../images/edit.png";
import del_img from "../images/del.png";
//import "bootstrap/dist/css/bootstrap.css";

class ShowQuestionsPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      Questions: [{ id: "", questionCont: "", isOpen: "" }],
      Answers: []
    };
  }

  componentDidMount() {
    axios.get("http://www.testapi.pl/api/questions/").then(res => {
      const Questions = res.data;
      this.setState({ Questions });
    });
  }
  // deleteTask =(id) =>{
  //   const Questions = [...this.state.Questions];
  //   const index = Questions.findIndex(Questions =>Questions.id ===id);
  //   Questions.
  // }

  // deleteHandler = itemId => {
  //   console.log("to: " + itemId);
  //   axios
  //     .delete("http://www.testapi.pl/api/questions/" + itemId)
  //     .then(response => {
  //       console.log(response);
  //     });
  // };

  render() {
    var idd = 1;
    const list = this.state.Questions.map(question => (
      <tr>
        <td>{idd++}</td>
        <td key={question.id}>{question.questionCont}</td>
        <td>
          <div className="buttons">
            <button className="crud">
              <Link to={`/questionspanel/wyswietl/${question.id}`}>
                <img className="crud" src={info_img} alt="" />
              </Link>
            </button>
            <button className="crud">
              <Link to={`/questionspanel/edycja/${question.id}`}>
                <img className="crud" src={edit_img} alt="" />
              </Link>
            </button>
            <button>
              {/* <button onClick={this.deleteHandler(question.id)}>  */}
              <Link to={`/questionspanel/usun/${question.id}`}>
                <img className="crud" src={del_img} alt="" />
              </Link>
            </button>
          </div>
        </td>
      </tr>
    ));
    return (
      <div className="ten">
        <table id="questions">
          <thead>
            <tr>
              <th>#</th>
              <th>Pytanie</th>
              <th>Opcje</th>
            </tr>
          </thead>
          <tbody>{list}</tbody>
        </table>
      </div>
    );
  }
}
export default ShowQuestionsPage;
