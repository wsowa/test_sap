import React, { Component } from "react";
import axios from "axios";

class TestPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //Questions:[{"id":1,"questionContent":"2+2","isOpen":0,"answers":[{"id":1,"answearContent":"4","isTrue":1},{"id":2,"answearContent":"5","isTrue":0}]},{"id":2,"questionContent":"3+3","isOpen":0,"answers":[{"id":7,"answearContent":"6","isTrue":1},{"id":8,"answearContent":"12","isTrue":0}]},{"id":4,"questionContent":"2+6","isOpen":0,"answers":[{"id":5,"answearContent":"4","isTrue":1},{"id":6,"answearContent":"8","isTrue":0}]}],
      Questions: [
        {
          id: 1,
          questionContent: "222+22",
          isOpen: 0,
          answers: [
            { id: 1, answearContent: "4", isTrue: 1 },
            { id: 2, answearContent: "5", isTrue: 0 }
          ]
        },
        { id: 2, questionContent: "3+3", isOpen: 0, answers: [] }
      ],
      //Questions: [],
      iter: 0,
      Answers: [
        {
          user: {
            id: 1,
            name: "User1"
          },
          question: {
            id: 2,
            questionContent: "3+3",
            isOpen: 0,
            answers: [
              {
                id: 7,
                answearContent: "6",
                isTrue: 1
              },
              {
                id: 8,
                answearContent: "12",
                isTrue: 0
              }
            ]
          },
          userAnswers: [
            {
              id: 2,
              answer: {
                id: 7,
                answearContent: "6",
                isTrue: 1
              }
            }
          ]
        }
      ]
    };
    this.handleClick = this.handleClick.bind(this);
  }
  // state = {
  //     Questions: [{"id":1,"questionContent":"2+2","isOpen":0,"answers":[{"id":1,"answearContent":"4","isTrue":1},{"id":2,"answearContent":"5","isTrue":0}]},{"id":2,"questionContent":"3+3","isOpen":0,"answers":[]}],
  //     //Questions: []
  //     iter: 0,
  //     handleClick = this.handleClick.bind(this)
  // };

  componentDidMount() {
    fetch("https://localhost:44308/api/questions")
      .then(res => res.json()) //{return res.json()})
      .then(data =>
        this.setState({
          Questions: data
        })
      );
    // axios.get("https://localhost:44308/api/questions")
    // .then(res => {
    //     const Questions = res.data;
    //    this.setState({Questions});
    // })
    //this.setState({Questions})
  }

  Result() {
    return this.state.Questions.map((rowdata, id) => (
      <div>
        {rowdata.questionContent}
        {typeof rowdata.answers == "object" ? (
          <div>
            {rowdata.answers.map((subRow, ii) => (
              <div>
                <div>{subRow.answearContent}</div>
              </div>
            ))}
          </div>
        ) : null}
      </div>
    ));
  }

  handleClick() {
    this.setState(state => ({
      iter: state.iter + 1
    }));
  }

  tooglechange = () => {
    var newAnswers = this.state.Answers.slice();
    //var usanswer = this.state.Questions[this.state.iter].answers[this.]
    var question = this.state.Questions[this.state.iter];
    newAnswers.push({
      user: {
        id: 1,
        name: "User1"
      },
      question
    });
    //this.state.Questions[this.state.iter])//("1")
    this.setState({
      Answers: newAnswers
    });
  };

  render() {
    return (
      <div>
        {this.state.Questions[0].questionContent}
        {this.state.Questions[this.state.iter].answers.map((row, ii) => (
          <div>
            {row.answearContent}
            <input
              type="checkbox"
              value={row.id}
              onChange={this.tooglechange}
            />
          </div>
        ))}
        <button onClick={this.handleClick}>next</button>
      </div>
    );
    // return(
    //     this.state.Questions.map((rowdata,id)=>
    //         <div>
    //             {rowdata.questionContent}
    //                 {
    //                     (typeof(rowdata.answers)=='object')?
    //                         <div>
    //                             {
    //                                 rowdata.answers.map((subRow,ii)=>
    //                                 <div>
    //                                     <div>{subRow.answearContent}</div>
    //                                 </div>
    //                                 )
    //                             }
    //                         </div>
    //                         :
    //                         null
    //                 }
    //         </div>
    //                 )
    //     )
  }
}
export default TestPage;
